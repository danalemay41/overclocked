﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DropoffPoint : MonoBehaviour
{
    [SerializeField] public PartScript.PartType accepts;
    [SerializeField] private List<MeshRenderer> visuals = new List<MeshRenderer>();
    public bool hasPart;

    [SerializeField] private float partSearchRadius = 0.3f;
    [SerializeField] private LayerMask partMask = Physics.AllLayers;
    [SerializeField] private PlayerScript myPlayer;

    private void Start()
    {
        References.instance.AddDropoff(this);
    }
    public void EnableVisuals()
    {
        foreach (var item in visuals)
        {
            item.gameObject.SetActive(true);
        }
    }
    public void DisableVisuals()
    {
        foreach (var item in visuals)
        {
            item.gameObject.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        if (hasPart)
            return;

        PartScript nearestPart = null;
        float distToNearest = Mathf.Infinity;

        foreach (Collider c in Physics.OverlapSphere(transform.position, partSearchRadius, partMask, QueryTriggerInteraction.Ignore))
        {
            PartScript part;
            float distToCollider = (transform.position - c.transform.position).sqrMagnitude;

            if (c.TryGetComponent(out part) && distToCollider < distToNearest)
            {
                if (accepts == part.partType)
                {
                    nearestPart = part;
                    distToNearest = distToCollider;
                }
            }
        }

        if (nearestPart)
        {
            if(myPlayer.partPoints.Contains(this))
            {
                nearestPart.EquipTo(transform);
            }
        }
    }
}
