﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalPlayer : MonoBehaviour
{
    public Transform head;
    public ControllerInput handLeft;
    public ControllerInput handRight;
}
