﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public GameObject PlayerPrefab;
    public LocalPlayer LocalPlayer;
    private bool PlayerSpawned = false;

    public bool IsMasterClient
    {
        get
        {
            return PhotonNetwork.IsMasterClient;
        }
    }

    private void Awake()
    {
        if(Instance)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    private void OnDestroy()
    {
        if(Instance == this)
        {
            Instance = null;
        }
    }

    void Update()
    {
        if (!RoomManager.Instance.isConnected)
            return;

        if (IsMasterClient)
            MasterClientOperations();

        LocalClientOperations();
    }

    public void MasterClientOperations()
    {

    }

    public void LocalClientOperations()
    {
        if(RoomManager.Instance.isConnected && !PlayerSpawned)
        {
            GameObject spawnedPlayer = PhotonNetwork.Instantiate(PlayerPrefab.name, transform.position, Quaternion.identity);
            spawnedPlayer.GetComponent<PlayerScript>().AssignToLocalPlayer(LocalPlayer);
            PlayerSpawned = true;
        }

        if(RoomManager.Instance.RoomProperties.ContainsKey("string_WinningPlayer"))
        {
            Debug.Log("GAME OVER");
            //it's over go home
            //figure out which player, announce they're a winner
            //end the game
            foreach(ControllerInput input in FindObjectsOfType<ControllerInput>())
            {
                input.enabled = false;
            }
        }
    }

    public void RobotComplete()
    {
        if(!RoomManager.Instance.RoomProperties.ContainsKey("string_WinningPlayer"))
        {
            ExitGames.Client.Photon.Hashtable newProperties = new ExitGames.Client.Photon.Hashtable
            {
                { "string_WinningPlayer", PhotonNetwork.LocalPlayer.UserId }
            };
            RoomManager.Instance.RoomProperties = newProperties;
        }
    }
}
