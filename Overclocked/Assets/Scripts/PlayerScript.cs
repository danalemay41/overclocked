﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public List<DropoffPoint> partPoints;
    public Transform head;
    public Transform handLeft;
    public Transform handRight;

    public int partsCompleted { get; private set; } = 0;

    //Also in this script: move to location?

    private void Update()
    {
        CheckforCompletedParts();
    }

    void CheckforCompletedParts()
    {
        partsCompleted = 0;
        for(int i = 0; i < partPoints.Count; i++)
        {
            if(partPoints[i].hasPart)
            {
                ++partsCompleted;
            }
        }

        if(partsCompleted >= partPoints.Count)
        {
            GameManager.Instance.RobotComplete();
        }
    }

    public void AssignToLocalPlayer(LocalPlayer player)
    {
        head.parent = player.head;
        head.localPosition = Vector3.zero;
        head.localRotation = Quaternion.identity;

        handLeft.parent = player.handLeft.transform;
        handLeft.localPosition = Vector3.zero;
        handLeft.localRotation = Quaternion.identity;
        player.handLeft.handParent = handLeft;

        handRight.parent = player.handRight.transform;
        handRight.localPosition = Vector3.zero;
        handRight.localRotation = Quaternion.identity;
        player.handRight.handParent = handRight;
    }
}
