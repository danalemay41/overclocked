﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class RoomManager : MonoBehaviourPunCallbacks
{

    public bool DebugLogRoomProperties = false;

    public static RoomManager Instance;

    protected string _roomName = "room";

    protected ExitGames.Client.Photon.Hashtable _roomProperties;
    public ExitGames.Client.Photon.Hashtable RoomProperties
    {
        get
        {
            return _roomProperties;
        }
        set
        {
            PhotonNetwork.CurrentRoom.SetCustomProperties(value);
        }
    }

    public bool RoomCanBeJoined
    {
        get
        {
            return PhotonNetwork.CurrentRoom.IsOpen;
        }
        set
        {
            PhotonNetwork.CurrentRoom.IsOpen = value;
        }
    }

    public bool RoomIsVisible
    {
        get
        {
            return PhotonNetwork.CurrentRoom.IsVisible;
        }
        set
        {
            PhotonNetwork.CurrentRoom.IsVisible = value;
        }
    }

    protected List<RoomInfo> _roomList;
    public List<RoomInfo> RoomList { get { return _roomList; } }

    public bool isConnected = false;

    private void Awake()
    {
        //Making this class a singleton
        if (Instance)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
        Debug.Log("Starting connection");
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Master connected to.");
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        if (_roomName != "")
        {
            //JoinRoom(_roomName);
            PhotonNetwork.JoinOrCreateRoom(_roomName, null, PhotonNetwork.CurrentLobby, null);
        }
    }

    [ContextMenu("Log Players")]
    private void LogPlayers()
    {
        string playerList = PhotonNetwork.CurrentRoom.Name + " " + PhotonNetwork.CurrentRoom.PlayerCount + " {";
        foreach (Player p in PhotonNetwork.PlayerList)
        {
            playerList += "\n\t" + p.UserId;
        }
        Debug.Log(playerList + "\n}");
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Room joined");
        isConnected = true;

        LogPlayers();

        _roomProperties = PhotonNetwork.CurrentRoom.CustomProperties;
    }

    public override void OnLeftRoom()
    {
        Debug.Log("Room left");

        _roomProperties = null;
        isConnected = false;
        TypedLobby lobby = PhotonNetwork.CurrentLobby;
        PhotonNetwork.LeaveLobby();
        PhotonNetwork.JoinLobby(lobby);
    }

    //public void JoinRoom(string room)
    //{
    //    _roomName = room;
    //    PhotonNetwork.JoinRoom(_roomName);
    //}

    //public void CreateRoom(string room)
    //{
    //    _roomName = room;
    //    PhotonNetwork.CreateRoom(_roomName, null, PhotonNetwork.CurrentLobby, null);
    //}

    public override void OnCreatedRoom()
    {
        Debug.Log("Created new room");
        PhotonNetwork.CurrentRoom.MaxPlayers = 2;
    }

    public void LeaveRoom()
    {
        if (!isConnected)
        {
            Debug.LogWarning("Can not leave room when there is no room connected to.");
            return;
        }

        Debug.Log("Leaving room...");
        PhotonNetwork.LeaveRoom();
    }

    public void ForceRefreshRoomList()
    {
        PhotonNetwork.GetCustomRoomList(PhotonNetwork.CurrentLobby, null);
    }

    public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        _roomProperties = PhotonNetwork.CurrentRoom.CustomProperties;

        if (DebugLogRoomProperties) Debug.Log("Room properties updating to " + _roomProperties.ToStringFull());
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);
        _roomList = roomList;

        string rooms = "rooms in " + PhotonNetwork.CurrentLobby.Name + " : {";
        foreach (RoomInfo rInfo in _roomList)
        {
            rooms += "\n\t" + rInfo.Name + " | " + rInfo.IsOpen + " | " + rInfo.MaxPlayers;
        }
        Debug.Log(rooms + "\n}");
    }
}