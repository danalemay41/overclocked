﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PartScript : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    

    public enum PartType
    {
        First,
        Gear,
        Lightbalb,
        Pipe,
        PipeBent,
        Last
    }
    [SerializeField] public PartType partType;

    public void EquipTo(Transform t)
    {
        PhotonView pView;
        if (TryGetComponent(out pView))
        {
            int i = t.gameObject.GetPhotonView().ViewID;
            pView.RPC("NewtworkEquip", RpcTarget.All, i);
        }
    }

    [PunRPC]
    public void NewtworkEquip(int transformID)
    {
        transform.parent = PhotonView.Find(transformID).transform;

        if (rb)
        {
            rb.isKinematic = true;
        }
        transform.localPosition = Vector3.forward * 0.1f;
        transform.localRotation = Quaternion.identity;
    }

    [PunRPC]
    public void NetworkUnequip()
    {
        if(rb)
        {
            rb.isKinematic = false;
        }

        transform.parent = null;
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (!rb)
            rb = GetComponent<Rigidbody>();
    }
#endif
}
