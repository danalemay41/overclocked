﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPositioning : MonoBehaviour
{
    public Transform head;
    public float bodyOffset = -2.5f;

    // Update is called once per frame
    void Update()
    {
        if (!head)
            return;

        transform.position = head.position + (Vector3.up * bodyOffset);
        transform.rotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(head.forward, Vector3.up), Vector3.up);
    }
}
