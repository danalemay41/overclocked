﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class References : MonoBehaviour
{
    public static References instance;
    public static GameObject Player
    {
        get
        {
            return instance.player;
        }
        set
        {
            instance.player = value;
        }
    }
    [SerializeField] private GameObject player;

    public static bool Menu
    {
        get
        {
            return instance.menu;
        }
        set
        {
            instance.menu = value;
        }
    }
    [SerializeField] private bool menu;

    public static List<DropoffPoint> Dropoffs
    {
        get
        {
            return instance.dropoffs;
        }
        set
        {
            instance.dropoffs = value;
        }
    }
    private List<DropoffPoint> dropoffs = new List<DropoffPoint>();

    public void AddDropoff(DropoffPoint aDropoff)
    {
        dropoffs.Add(aDropoff);
    }

    public void Highlight(PartScript.PartType aType)
    {
        foreach (var item in dropoffs)
        {
            if(item.accepts == aType)
            {
                item.EnableVisuals();
            }
            else
            {
                item.DisableVisuals();
            }
        }
    }
    private void Awake()
    {
        if (null != instance)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
