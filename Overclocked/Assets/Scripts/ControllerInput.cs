﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Photon.Pun;
//using UnityEngine.AI;

public class ControllerInput : MonoBehaviour
{
    #region References
    [SerializeField] private SteamVR_Input_Sources handType; // 1
    [SerializeField] private SteamVR_Action_Boolean Teleport; // 2
    [SerializeField] private SteamVR_Action_Boolean GrabGrip; // 3
    [SerializeField] private SteamVR_Action_Boolean GrabPinch; // 4
    [HideInInspector] private PartScript heldObject;
    [HideInInspector] private LineRenderer lineRender;
    [SerializeField] private float grabRadius = 0.1f;
    [SerializeField] private LayerMask grabMask = Physics.AllLayers;

    public Transform handParent;

    #endregion
    #region Variables
    #endregion
    private void Start()
    {
        GameObject temp = new GameObject();
        lineRender = temp.AddComponent<LineRenderer>();
        lineRender.startWidth = 0.01f;
        lineRender.endWidth = 0.01f;
    }
    void Update()
    {
        if (References.Menu)
        {
            MenuTargeting();
        }
        else if (lineRender.GetPosition(0) != lineRender.GetPosition(1))
        {
            lineRender.SetPosition(0, transform.position);
            lineRender.SetPosition(1, transform.position);
        }

        //Need that highlighting on local

        if (!handParent)
            return;

        if (GetGrabPinchDown())
        {
            PartScript part = GetPartByHand();
            if (part != heldObject)
            {
                if (heldObject)
                {
                    PhotonView pView;
                    if (heldObject.TryGetComponent(out pView))
                    {
                        pView.RPC("NetworkUnequip", RpcTarget.All);
                    }
                }
                if (part)
                {
                    part.EquipTo(handParent);
                }

                heldObject = part;
            }
        }
        if (GetGrabPinchUp() && heldObject)
        {
            PhotonView pView;
            if (heldObject.TryGetComponent(out pView))
            {
                pView.RPC("NetworkUnequip", RpcTarget.All);
            }
            heldObject = null;
        }
    }//end update

    private PartScript GetPartByHand()
    {
        PartScript nearestPart = null;
        float distToNearest = Mathf.Infinity;

        foreach (Collider c in Physics.OverlapSphere(transform.position, grabRadius, grabMask, QueryTriggerInteraction.Ignore))
        {
            PartScript part;
            float distToCollider = (transform.position - c.transform.position).sqrMagnitude;

            if (c.TryGetComponent(out part) && distToCollider < distToNearest)
            {
                nearestPart = part;
                distToNearest = distToCollider;
            }
        }
        return nearestPart;
    }

    public void MenuTargeting()
    {
        lineRender.SetPosition(0, transform.position);

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 5))
        {
            lineRender.SetPosition(1, hit.point);
        }
        else
        {
            lineRender.SetPosition(1, transform.forward * 100);
        }
    }

    #region InputEvents
    public bool GetTeleportDown()
    {
        return Teleport.GetStateDown(handType);
    }
    public bool GetTeleportUp()
    {
        return Teleport.GetStateUp(handType);
    }
    public bool GetGrabGripDown()
    {
        return GrabGrip.GetStateDown(handType);
    }
    public bool GetGrabGrip()
    {
        return GrabGrip.GetState(handType);
    }
    public bool GetGrabGripUp()
    {
        return GrabGrip.GetStateUp(handType);
    }
    public bool GetGrabPinchDown()
    {
        return GrabPinch.GetStateDown(handType);
    }
    public bool GetGrabPinch()
    {
        return GrabPinch.GetState(handType);
    }
    public bool GetGrabPinchUp()
    {
        return GrabPinch.GetStateUp(handType);
    }
    #endregion

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, grabRadius);
    }
#endif
}